package com.docker02springboot.docker02springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Docker02SpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Docker02SpringBootApplication.class, args);
	}

}
