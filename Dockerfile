FROM openjdk:12
ADD target/docker02-spring-boot.jar docker02-spring-boot.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker02-spring-boot.jar"]